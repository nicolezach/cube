import { Flex, Text, VStack, Grid, HStack, Input, ChakraProvider, GridItem, Button, Checkbox, Spacer } from "@chakra-ui/react";

const colors = { 0: "#121212", 1: "#f94144", 2: "#f3722c", 3: "#f8961e", 4: "#f9c74f", 5: "#90be6d", 6: "#43aa8b" }

export default function CubeList({cubes}) {
    return (
        <Grid
            p={4}
            w="100%"
            h="60%"
            bg="#292929"
            templateColumns="repeat(20, 1fr)"
            gap={10}
            borderRadius={5}
            justifyItems={"center"}
            alignContent={"center"}
          >

            {cubes.map((options, i) => {
              return <GridItem w="100%" h="100%" key={i}>
                <Flex w="50px" h="50px" opacity={options.terminated && 0.2} borderRadius={6} bg={colors[options.val]} justifyContent={"center"} alignItems="center">
                  <Text color="white" fontWeight={"medium"} fontSize={"lg"}>{options.val}</Text>
                </Flex>
              </GridItem>
            })}
          </Grid>
    )
}