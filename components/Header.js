import { Flex, Text, VStack, Grid, HStack, Input, ChakraProvider, GridItem, Button, Checkbox, Spacer } from "@chakra-ui/react";
import { useEffect, useState } from "react";


export default function Header(props) {
    return (
        <HStack w="100%" h="7vh">
            <Text fontSize="4xl" fontWeight={"bold"} color="white">Cube Simulator</Text>
            <Spacer />
            <VStack spacing={0} alignItems={"center"} justifyContent={"center"} w="5%" h="90%" borderRadius={7} bg="#292929" mt={2} mr={3}>
                <Text color={"white"} fontWeight={"bold"} fontSize={"lg"}>{props.counter}</Text>
                <Text color={"white"} fontSize={"md"}>Cubes left</Text>
            </VStack>
        </HStack>
    )
}