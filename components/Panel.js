import { Flex, Text, VStack, Grid, HStack, Input, ChakraProvider, GridItem, Button, Checkbox, Spacer } from "@chakra-ui/react";
import { useEffect, useState } from "react";

export default function Panel(props) {
    return (
        <VStack p={3} w="100%" h="21%" alignItems={"start"} justifyContent={"start"} spacing={2} borderRadius={7} border="1px solid #494949">
            <Text color="white" fontWeight={"medium"} fontSize={"xl"}>Control Panel:</Text>
            <Text color="white" fontWeight={"normal"} fontSize={"md"}>Conditions</Text>
            <HStack pl={1} spacing={6}>
                {
                    [...Array(6)].map((_, i) => {
                        return <Checkbox onChange={() => props.check(i + 1)} key={i} colorScheme={"black"}><Text color="white">{i + 1}</Text></Checkbox>
                    })
                }
            </HStack>
            <HStack pt={3}>
                <Button onClick={props.handleRoll}>Roll!</Button>
                <Button _hover={{background: "transparent"}} color="white" border="1px solid #FFFFFF" bg="transparent" onClick={props.resetAll}>Reset</Button>
            </HStack>
        </VStack>
    )
}