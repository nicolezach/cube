import { Flex, Text, VStack, Grid, HStack, Input, ChakraProvider, GridItem, Button, Checkbox, Spacer } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import CubeList from "../components/CubeList";
import Header from "../components/Header";
import Panel from "../components/Panel";

function generateRandomIntegerInRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


export default function Home() {
  const [cubes, setCubes] = useState([...Array(100)].map(() => ({ val: 0, terminated: false })));
  const [conditions, setConditions] = useState([])
  const [counter, setCounter] = useState(100)

  const handleRoll = () => {
    setCubes(c => c.map(({ val, terminated }, i) => {
      return { val: (terminated ? val : generateRandomIntegerInRange(1, 6)), terminated: terminated }
    }))

    setCubes(c => c.map(({ val, terminated }, i) => {
      return { val: val, terminated: (conditions.includes(val)) }
    }))
  }

  useEffect(() => {
    setCounter(() => 100 - cubes.filter(a => a.terminated).length);
  }, [cubes]);

  const check = (i) => {
    if (conditions.find(e => e === i)) {
      setConditions(a => a.filter(e => e !== i))
    } else {
      setConditions(a => [...a, i])
    }
  }

  const resetAll = () => {
    setCubes([...Array(100)].map(() => ({ val: 0, terminated: false })))
    setConditions([])
    setCounter(100)
  }


  return (
    <Flex bg={"#121212"} w="100vw" h="100vh" alignItems={"center"} justifyContent={"center"}>
      <Flex bg={"#1E1E1E"} w="95vw" h="90vh" borderRadius={5}>
        <VStack spacing={5} w="100%" h="100%" alignItems={"start"} justifyContent={"start"} mt={3} mb={3} ml={5} mr={5}>
          <Header counter={counter}/>
          <Panel check={check} handleRoll={handleRoll} resetAll={resetAll} />
          <CubeList cubes={cubes} />
        </VStack>
      </Flex>
    </Flex>

  )
}
